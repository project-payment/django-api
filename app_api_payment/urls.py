from django.urls import path
from .views import HiWork

urlpatterns = [
    path('api/', HiWork.as_view(), name="api"),
]