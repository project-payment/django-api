from django.apps import AppConfig


class AppApiPaymentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_api_payment'
